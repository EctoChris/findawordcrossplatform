import { Component } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { TitlePage } from '../pages/title/title';
import { LoginPage } from '../pages/login/login';
import { HighscoresPage } from '../pages/highscores/highscores';

//So the observer can constantly check the status of authentication
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
// rootPage:any = HomePage;
//rootPage:any = TitlePage;
 rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public afAuth: AngularFireAuth) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    // //Initialize auth observer here:
    // this.afAuth.authState.subscribe(
    // (user) => {
    //   if (user)
    //   {
    //     //user is authenticated
    //    this.rootPage = LoginPage;
    //   }
    //   else {
    //     //user is not authenticated
    //     this.rootPage = TitlePage;
    //   }
    // });
  }
}
