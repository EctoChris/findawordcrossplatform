import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';

import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TitlePage } from '../pages/title/title';
import { LoginPage } from '../pages/login/login';
import { HighscoresPage } from '../pages/highscores/highscores';
import { LetterUtilProvider } from '../providers/letter-util/letter-util';
import { HttpClientModule } from '@angular/common/http';
import { DatabaseProvider } from '../providers/database/database';

//Firebase Imports:
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

//Environment.ts import
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TitlePage,
    LoginPage,
    HighscoresPage,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    IonicModule.forRoot(MyApp,{
  scrollPadding: false,
  scrollAssist: false
}),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    TitlePage,
    HighscoresPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LetterUtilProvider,
    DatabaseProvider,
  ]
})
export class AppModule {}
