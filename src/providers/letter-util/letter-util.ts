import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the LetterUtilProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LetterUtilProvider {
    // private letters = [26];
   private letters = [];

   // private randomLetters = [26];

  constructor(public http: HttpClient)
  {
    let counter:number = 0;
    //let numericalValue = 0;
    while (counter < 26)
    {
        var chr = String.fromCharCode(97 + counter);
        this.letters[counter] = chr;
        counter++;
    }
  }

  public getRandomLetter()
  {
     return this.letters[Math.floor(Math.random() * 26) + 0];
    //return '0';
  }

  public findPossibleDirections()
  {

  }
}
