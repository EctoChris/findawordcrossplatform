import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import asyncLoop from 'node-async-loop';

//Firebase Imports:
import {AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';


@Injectable()
export class DatabaseProvider {
   // Get a reference to the database service
   //public database;
   private letters = [];
   public user:any = null;
   private words = [];
   private arrayFromDictionary = [];

  constructor(public http: HttpClient, public afAuth:AngularFireAuth) {
      //database = firebase.database();
      let counter:number = 0;
      //let numericalValue = 0;
      while (counter < 26)
      {
          var chr = String.fromCharCode(97 + counter);
          this.letters[counter] = chr;
          counter++;
      }
  }

  public setUser(user:any) {
    this.user = user;
  }

  public getUser(){
    return this.user;
  }

   getRandomLetter(){
     return this.letters[Math.floor(Math.random() * 26) + 0];
  }

  placeInitialWord(){

  }

  // async getArrayOfWordsFromChar(char){
  //   var tempArray = [];
  //   const response = await firebase.database().ref('/' + this.getRandomLetter() + 'Words/').once('value');
  //   return this.snapshotToArray(response);
  //   //   firebase.database().ref('/' + char + 'Words/').once('value').then((snapshot) => {
  //   //   tempArray =  this.snapshotToArray(snapshot);
  //   // });
  //   // return tempArray;
  // }

  async getRandomArrayOfWords(firstChar){
    var tempArray = [];
    if (firstChar == '0')
    {
      const response = await firebase.database().ref('/' + this.getRandomLetter() + 'Words/').once('value');
      return this.snapshotToArray(response);
    }
    else
    {
      const response = await firebase.database().ref('/' + firstChar + 'Words/').once('value');
      return this.snapshotToArray(response);
    }
    // firebase.database().ref('/' + this.getRandomLetter() + 'Words/').once('value').then((snapshot) =>{
    //   callback(this.snapshotToArray(snapshot)); ;
    // });
  }

   getHighScores(callback){
     firebase.database().ref('/highscores').once('value').then((snapshot) => {
       console.log(snapshot);
       callback(this.snapshotTo2DArray(snapshot));
     });
  }

  deleteHighScore(userScore, callback){
    let path = '/highscores/' + userScore;
    firebase.database().ref(path).set(null).then(callback());
  }

//   deleteNote( id, userid, callback ){
//   let path = '/userProfile/' +userid + '/notes/' + id;
//   firebase.database().ref(path).set(null).then( callback() );
// }

  createNewHighScore(username, userScore, callback){
     let path = '/highscores';
     firebase.database().ref(path).child(<string> userScore).set(username).then(callback());
  }
  // createNote(data,userid){
  //     //create a new note usin the class in /models/note.ts and the data passed from home.ts
  //     let note = new Note( data.title, data.text);
  //     let path = '/userProfile/' +userid + '/notes/';
  //     //create a path to store notes under the current user's profile
  //     //write the note object using its created string as a key (leave child blank and firebase will auto generate an id)
  //     firebase.database().ref(path).child( <string> note.created).set(note);
  //   }


//   deleteNote( id, userid, callback ){
//   let path = '/userProfile/' +userid + '/notes/' + id;
//   firebase.database().ref(path).set(null).then( callback() );
// }

  readData() {
    console.log('reading data...');

    for(var i: number = 0; i < 3; i++)
    {
      var tempArray = [];
      firebase.database().ref('/' + this.getRandomLetter() + 'Words/').once('value').then((snapshot) =>{
          tempArray = this.snapshotToArray(snapshot);
          //console.log('the length of the array is : ' + tempArray.length);
          //console.log(tempArray);
          this.words.push(tempArray[Math.floor(Math.random() * tempArray.length) + 0]);
      });
    }

    //console.log(this.words);

    // return firebase.database().ref('/aWords/').once('value').then((snapshot) => {
    //      console.log(this.snapshotToArray(snapshot));
    //    });


    }

    snapshotToArray(snapshot) {
        var returnArr = [];
        snapshot.forEach((childSnapshot) => {
            var item = childSnapshot.val();

            returnArr.push(item);
        });
        return returnArr;
    }

    snapshotTo2DArray(snapshot){
      var returnArr = [];
      snapshot.forEach((childSnapshot) => {
        var innerArray = [];
        var key = childSnapshot.key;

      //  innerArray.push(childSnapshot);
        innerArray.push(childSnapshot.key);
        innerArray.push(childSnapshot.val());
        returnArr.push(innerArray);
      })
      return returnArr;
    }

  readFromFile(){

  }

}
