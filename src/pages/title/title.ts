import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { HighscoresPage } from '../highscores/highscores';
import { DatabaseProvider } from '../../providers/database/database';
import { AlertController } from 'ionic-angular';

//Firebase Imports:
import {AngularFireAuth } from 'angularfire2/auth';

@IonicPage()
@Component({
  selector: 'page-title',
  templateUrl: 'title.html',
})
export class TitlePage {

  private email:string = "";
  private password:string = "password";
  public user:any;
  private usernameExists:boolean;
  private username:string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public afAuth:AngularFireAuth,
     public databaseProvider:DatabaseProvider, public alertCtrl: AlertController) {
    console.log(this.databaseProvider.getUser());
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TitlePage');

    //if (this.databaseProvider.getUser().displayName == null)
    if(this.afAuth.auth.currentUser.displayName == null)
    {
      this.usernameExists = false;
      console.log('display name does not exist');
    }
    else {
      this.usernameExists = true;
      console.log('user has a display name');
      this.username = this.afAuth.auth.currentUser.displayName;
    }
  }

  showInternetAlert() {
     const alert = this.alertCtrl.create({
     title: 'A Problem Occurred',
     subTitle: 'Please test your internet connection',
     buttons: ['OK']
   });
   alert.present();
   }

   showUsernameAlert() {
      const alert = this.alertCtrl.create({
      title: 'Invalid Username',
      subTitle: 'Username must be between 3 and 11 characters long!',
      buttons: ['OK'],
      cssClass: 'alertCustomCss' // <- added this
    });
    alert.present();
    }

  navigateToGame(){
    if(this.usernameExists == true)
    {
      this.navCtrl.push(HomePage);
    }
    else if (this.usernameExists == false && this.username.length < 12 && this.username.length > 3)
    {
      //update user info (displayName)
      var user = this.afAuth.auth.currentUser;

      user.updateProfile({
      displayName: this.username, photoURL:null }).then((updated) => {
          // Update successful.
          console.log('yeww updated');
          console.log(user.displayName);
      }).catch((error) => {
          // An error happened.
        this.showInternetAlert();
        //  this.errorLabel = "lost internet connection, please try again";
      });

      // this.afAuth.auth.createUserWithEmailAndPassword(this.email, this.password).then((logIn) => {
      //  this.databaseProvider.getUser().displayName = this.username;

      this.navCtrl.push(HomePage);
      console.log(this.databaseProvider.getUser().displayName);
    }
    else {
      //Username must be between 3 and 8 characters long
      this.showUsernameAlert();
    }
  }

  navigateToHighscores(){
    this.navCtrl.push(HighscoresPage);
  }
}
