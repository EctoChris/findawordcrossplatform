import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { AngularFireAuth } from 'angularfire2/auth';


@IonicPage()
@Component({
  selector: 'page-highscores',
  templateUrl: 'highscores.html',
})
export class HighscoresPage {
  public highScores = [];
  public username:string = "";
  constructor(public navCtrl: NavController, public navParams: NavParams, private databaseProvider: DatabaseProvider,
            public afAuth:AngularFireAuth) {

        this.username = this.afAuth.auth.currentUser.displayName;
        this.databaseProvider.getHighScores((highScoresArray) => {
              if(highScoresArray)
              {
                  this.highScores = highScoresArray;
                  console.log(this.highScores);

                  this.sortHighScores();
              }
              else
              {
                  console.log('empty array');
              }
            });
          }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HighscoresPage');
  }

  sortHighScores(){
    console.log('now sorting');
    var swapped:boolean;
    do {
        swapped = false;
        for (var i=0; i < this.highScores.length-1; i++) {
            if (+this.highScores[i][0] < +this.highScores[i+1][0]) {
                var temp = this.highScores[i];
                this.highScores[i] = this.highScores[i+1];
                this.highScores[i+1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
    for (let i:number = 0; i < this.highScores.length; i++)
    {
      console.log(this.highScores[i][0]);
    }
  }

   navigateToTitle(){
     this.navCtrl.pop();
   }
}
