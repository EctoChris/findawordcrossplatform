import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HighscoresPage } from './highscores';

@NgModule({
  declarations: [
    HighscoresPage,
  ],
  imports: [
    IonicPageModule.forChild(HighscoresPage),
  ],
})
export class HighscoresPageModule {}
