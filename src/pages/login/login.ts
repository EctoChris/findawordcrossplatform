import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TitlePage } from '../title/title';
import { DatabaseProvider } from '../../providers/database/database';

//Firebase Imports:
import {AngularFireAuth } from 'angularfire2/auth';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private email:string = "ziggy@ziggy.com";
  private password:string = "ziggyro";
  public user:any;
  private errorLabel:string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams,  public afAuth:AngularFireAuth, private databaseProvider: DatabaseProvider) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  goToTitlePage()  {
    this.signUp();
  }


  signUp(){
    //this.afAuth.auth.createUserWithEmailAndPassword(this.email, this.password).catch((error) => {
      this.afAuth.auth.createUserWithEmailAndPassword(this.email, this.password).then((logIn) => {
      this.afAuth.auth.signInWithEmailAndPassword(this.email, this.password).then((result) => {
        this.databaseProvider.setUser(result);
        console.log(this.databaseProvider.getUser());
      })
      }).then ((goToNextPage) => {
        this.navCtrl.push(TitlePage);
      }).catch((error) => {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      if (errorCode ==   'auth/email-already-in-use')
      {
          this.signIn();
      } else if ('auth/wrong-password')
      {
          this.errorLabel = "Invalid Password, already an account";
      } else
      {
          this.errorLabel = "Invalid input format";
      }
      console.log(error);
    });
  }


  signIn(){
      this.afAuth.auth.signInWithEmailAndPassword(this.email, this.password)
        .then((result) => {
        //   this.user = result;
           this.databaseProvider.setUser(result);
           console.log(this.databaseProvider.getUser());
         }).then ((goToNextPage) => {
            this.navCtrl.push(TitlePage);
         }).catch((error) => {
           // Handle Errors here.
           var errorCode = error.code;
           var errorMessage = error.message;

           if (errorCode == 'auth/wrong-password')
           {
               this.errorLabel = "Invalid Password, already an account";
           } else
           {
               this.errorLabel = "Invalid input format";
           }
         });
  }

}
