import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import asyncLoop from 'node-async-loop';
import { LetterUtilProvider } from '../../providers/letter-util/letter-util';
import { Letter } from '../../models/Letter';
import { Word } from '../../models/Word';
import { TitlePage } from '../title/title';
import { DatabaseProvider } from '../../providers/database/database';
import { AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { HighscoresPage } from '../highscores/highscores';


enum Direction {
    UP = "UP",
    TOPRIGHT = "TOPRIGHT",
    RIGHT = "RIGHT",
    BOTTOMRIGHT = "BOTTOMRIGHT",
    DOWN = "DOWN",
    BOTTOMLEFT = "BOTTOMLEFT",
    LEFT = "LEFT",
    TOPLEFT = "TOPLEFT"
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
   private randomLetters = [];
   public rows =    [0,1,2,3,4,5,6,7,8,9,10,11];
   public columns = [0,1,2,3,4,5,6,7,8,9,10,11];
   public wordRows = [0,1,2];
   public wordColumns = [0,1,2];
   public done:boolean = false;
   public mouseIsDown:boolean = false;
   public madeHighScores:boolean = false;
   public algorithmComplete:boolean;
   public letters = [];
   public wordsToFind:Array<Word> = [];
   public tempLetters:Array<Letter> = [];
   public tempPathLetters:Array<Letter> = [];
   public xCoord:number;
   public yCoord:number;
   public tempString:string = null;
   public possibleDirections:Array<Direction> = [];
   public playerScore:number;
   public timeLeft:string = "0:00";
   public loadingMessage:string ="";
   public secondsLeft:number;
   public timer;

//Add this to constructor: 'private letterUtil: LetterUtilProvider'
 constructor(public navCtrl: NavController, private letterUtil: LetterUtilProvider,
            private databaseProvider: DatabaseProvider, public afAuth:AngularFireAuth,
          public alertCtrl: AlertController){
      let counter = 0;
      while (counter < 144)
      {
        this.randomLetters.push(this.letterUtil.getRandomLetter());
        counter++;
      }
      this.loadingMessage = "Generating Game..";
      this.playerScore = 0;
      this.initialize();
      this.startCountDown();

  }

  private initialize() {
    //initializing 2 dimensional array
    for(let i: number = 0; i < 12; i++)
    {
            this.letters[i] = [12];
            for(let j: number = 0; j < 12; j++)
            {
                this.letters[i][j] = new Letter(this.letterUtil, i, j);
            }
    }

    for (let i = 0; i < 9; i++)
    {
      this.wordsToFind[i] = new Word();
    }


    //  this.databaseProvider.readData(); --- DO NOT DELETE THIS WORKING METHOD

    //for (let i = 0; i < 3; i++)
    asyncLoop([0,1,2,3,4,5,6,7,8], async (i, next) =>
    {
       console.log('Iteration of for loop #No: ' + i);
       let tempArray = [];
       this.algorithmComplete = false;
       let test02:boolean = true;
       let newWordsArray = [];
       let randomWord = "";
       this.secondsLeft = 180;
        do {
           this.tempPathLetters = [];

        //New Test Logic:
        //Step 1: pick a random coordinate on the grid (row, col);
        let row:number  = Math.floor(Math.random() * 12) + 0;
        let col:number  = Math.floor(Math.random() * 12) + 0;

        //Step 2: pick a random direction from available directions (must be 4 or greater).
        let totalPossibleDirections:Array<Direction> = []
        totalPossibleDirections = this.findAvailableDirections(row, col);
        let randomDirection:Direction = totalPossibleDirections[Math.floor(Math.random() * totalPossibleDirections.length) + 0];
        console.log("The location on grid = " + row + ", " + col + " and randomly chosen direction is: " + randomDirection);

        //Step 3: grab letters on path and save into array
        this.getLettersFromPath(row, col, randomDirection);

        //Step 4: Get string of letters from tempPathLetters:
        let stringOfLetters = this.getStringFromPath();

        //Step 4: Get first character of tempLetters:
        let firstChar = stringOfLetters[0];

             //Step 4a.1: Randomize character and return array of words from dictionary
             let array = [];
             try {
                array = await this.databaseProvider.getRandomArrayOfWords(firstChar);
              } catch(error) {
                console.log('some shit happen', error);
              }

            let tempTest = array[0];

             //Step 4a.2: Traverse array and remove elements that are too long or short
             for (let k:number = 0; k< array.length; k++)
             {
               let tempString = array[k];

               if (tempString.length >= 4 && tempString.length <= stringOfLetters.length)
               {

                test02 = true;
                for(let j:number = 0; j < stringOfLetters.length; j++)
                {
                  if(stringOfLetters[j] != '0')
                  {
                    if(stringOfLetters[j] != tempString[j])
                    {
                      test02 = false;
                    }
                  }
                }
                if (test02 == true)
                {
                  newWordsArray.push(array[k]);
                }
               }
             }

             //Step 4a.3: Place random word on path:
             randomWord = newWordsArray[Math.floor(Math.random() * newWordsArray.length) + 0]
             console.log('current length of newWordsArray == ' + newWordsArray.length);
           } while(randomWord == null || randomWord.length == 0);

             this.placeWordOnPath(randomWord);
             this.wordsToFind[i].setWordString(randomWord);
             console.log(this.wordsToFind[i].getWordString());
             next();
      }, (test) =>
    {
      this.fillInRemainingLetters();
      this.algorithmComplete = true;
    });

  }

  fillInRemainingLetters(){
    for(let i: number = 0; i < 12; i++)
    {
          for(let j: number = 0; j < 12; j++)
          {
              if(this.letters[i][j].getChar() == '0')
              {
                    this.letters[i][j].setChar(this.letterUtil.getRandomLetter());
              }
          }
    }

  }

  startCountDown(){
    this.timer = setInterval((countdown) => {
      this.secondsLeft -= 1;
      this.displayTime(this.secondsLeft);
      if(this.secondsLeft == 0)
      {
        console.log('done');
        clearInterval(this.timer);
        this.checkHighScores();

        //GameOver Logic goes here:
      }
    }, 1000);
  }

  displayTime(totalSeconds){
      let minutes:number = Math.trunc(totalSeconds / 60);
      totalSeconds = totalSeconds - (minutes * 60);
      let tensOfSeconds:number = Math.trunc(totalSeconds/10);
      totalSeconds -= (tensOfSeconds * 10);
      let seconds: number = totalSeconds;
      this.timeLeft = minutes +":"+tensOfSeconds+""+seconds;
  }
  removeElementFromNewWordsArray(stringOfLetters, newWordsArray){
    for(let i:number = 0; i < newWordsArray.length; i++)
    {
      if (newWordsArray[i] == stringOfLetters)
      {
        console.log('removed this word = ' + newWordsArray[i] );
        newWordsArray.splice(i, 1);
      }
    }
    return newWordsArray;
  }

  placeWordOnPath(randomWord){
    console.log("Random word = " + randomWord);
    if(randomWord == null)
      return;
    for(let i:number = 0; i < randomWord.length; i++)
    {
      if(this.tempPathLetters[i] == randomWord[i])
      {
        continue;
      }
       this.setCorrespondingChar(this.tempPathLetters[i].getX(), this.tempPathLetters[i].getY(), randomWord[i]);
    }
  }

  getStringFromPath(){
    let tempString = "";
    for(let i:number = 0; i < this.tempPathLetters.length; i++)
    {
       tempString +=  this.getCorrespondingChar(this.tempPathLetters[i].getX(), this.tempPathLetters[i].getY());
    }
    console.log('temp = ' + tempString);
    return tempString;
  }

  setCorrespondingChar(x, y, char){
    this.letters[x][y].setChar(char);
  //  this.letters[x][y].highlight();
  }
  getCorrespondingChar(x, y){
    return this.letters[x][y].getChar();
  }

  isWordPlacingAlgorithmComplete(){
    if (this.algorithmComplete == true)
       return true;
    return false;
  }
  getLettersFromPath(row, col, direction){

    switch(direction)
    {
      case Direction.UP:
      {
        let diff = Math.abs(row - 0);
        for(let i:number = 0; i < diff; i++)
        {
          this.tempPathLetters[i] = new Letter(this.letterUtil, row, col);
          row--;
        }
        break;
      }
      case Direction.TOPRIGHT:
      {
        let rowDiff = Math.abs(row - 0);
        let colDiff = Math.abs(col - 12);
        let lowestDiff = this.compareDiffs(rowDiff, colDiff);
        for (let i:number = 0; i < lowestDiff; i++)
        {
          this.tempPathLetters[i] = new Letter (this.letterUtil, row, col);
          row--;
          col++;
        }
        break;
      }
      case Direction.RIGHT:
      {
        let diff = Math.abs(col - 12);
        for (let i:number = 0; i < diff; i++)
        {
          this.tempPathLetters[i] = new Letter (this.letterUtil, row, col);
          col++;
        }
        break;
      }
      case Direction.BOTTOMRIGHT:
      {
        let rowDiff = Math.abs(row - 12);
        let colDiff = Math.abs(col - 12);
        let lowestDiff = this.compareDiffs(rowDiff, colDiff);

        for (let i: number = 0; i < lowestDiff; i++)
        {
          this.tempPathLetters[i] = new Letter (this.letterUtil, row, col);
          row++;
          col++;
        }
        break;
      }
      case Direction.DOWN:
      {
        let diff = Math.abs(row - 12);
        for (let i:number = 0; i < diff; i++)
        {
          this.tempPathLetters[i] = new Letter(this.letterUtil, row, col);
          row++;
        }
        break;
      }
      case Direction.BOTTOMLEFT:
      {
        let rowDiff = Math.abs(row - 12);
        let colDiff = Math.abs(col - 0);
        let lowestDiff = this.compareDiffs(rowDiff, colDiff);

        for (let i:number = 0; i < lowestDiff; i++)
        {
          this.tempPathLetters[i] = new Letter(this.letterUtil, row,col);
          col--;
          row++;
        }
        break;
      }
      case Direction.LEFT:
      {
        let diff = Math.abs(col - 0);
        for(let i:number = 0; i < diff; i++)
        {
          this.tempPathLetters[i] = new Letter (this.letterUtil, row, col);
          col--;
        }
        break;
      }
      case Direction.TOPLEFT:
      {
        let rowDiff = Math.abs(row - 0);
        let colDiff = Math.abs(col - 0);
        let lowestDiff = this.compareDiffs(rowDiff, colDiff);
        for (let i:number = 0; i< lowestDiff; i++)
        {
          this.tempPathLetters[i] = new Letter (this.letterUtil, row, col);
          row--;
          col--;
        }
        break;
      }
    }
    // console.log("The letter path includes:");
    // for(let i:number = 0; i < this.tempPathLetters.length; i++)
    // {
    //   console.log("X = " + this.tempPathLetters[i].getX() + " Y = " + this.tempPathLetters[i].getY());
    // }


  }

  compareDiffs(number1, number2){
    if (number1 < number2 )
      return number1;
    else
      return number2;
  }

  findAvailableDirections(row, col)  {
    let colPossibleDirections:Array<Direction> = []
    let rowPossibleDirections:Array<Direction> = [];
    let totalPossibleDirections:Array<Direction> = []

    switch (col)
    {
      case 0:
      case 1:
      case 2:
        colPossibleDirections.push(Direction.UP, Direction.TOPRIGHT, Direction.RIGHT, Direction.BOTTOMRIGHT, Direction.DOWN);
        break;
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
        colPossibleDirections.push(Direction.UP, Direction.TOPRIGHT, Direction.RIGHT, Direction.BOTTOMRIGHT, Direction.DOWN, Direction.BOTTOMLEFT, Direction.LEFT, Direction.TOPLEFT);
        break;
      case 9:
      case 10:
      case 11:
        colPossibleDirections.push(Direction.UP, Direction.DOWN, Direction.BOTTOMLEFT, Direction.LEFT, Direction.TOPLEFT);
        break;
    }

    switch (row)
    {
      case 0:
      case 1:
      case 2:
        rowPossibleDirections.push(Direction.RIGHT, Direction.BOTTOMRIGHT, Direction.DOWN, Direction.BOTTOMLEFT, Direction.LEFT);
        break;
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
        rowPossibleDirections.push(Direction.UP, Direction.TOPRIGHT, Direction.RIGHT, Direction.BOTTOMRIGHT, Direction.DOWN, Direction.BOTTOMLEFT, Direction.LEFT, Direction.TOPLEFT);
        break;
      case 9:
      case 10:
      case 11:
        rowPossibleDirections.push(Direction.UP, Direction.TOPRIGHT, Direction.RIGHT, Direction.LEFT, Direction.TOPLEFT);
        break;
    }

    for(let i: number = 0; i < rowPossibleDirections.length; i++)
    {
      if (colPossibleDirections.includes(rowPossibleDirections[i]))
           totalPossibleDirections.push(rowPossibleDirections[i]);
    }
    // for (let i:number = 0; i < totalPossibleDirections.length; i++)
    // {
    //   console.log(totalPossibleDirections[i]);
    // }
    return totalPossibleDirections;
  }


  istempStringEmpty(){
    if (this.tempLetters.length != 0)
    return false;
    else
      return true;
  }
  clickedLetter(i, j){
    console.log("clicked "  + i + j);
  }

  mouseMove(event){
    //console.log(event);
    //this.xCoord = event.x;
    //this.yCoord = event.y;
    //console.log (event.x + event.y);
  }

  mouseDown(){
      console.log('mouse is down');
      this.mouseIsDown = true;
  }

  mouseUp(){
    //To Print string || compare with words from database
    let str:string = "";
    for (let d = 0; d < this.tempLetters.length; d++)
    {
      str += this.tempLetters[d].getChar();
    }
    console.log(str);

    for(let i = 0; i < this.wordsToFind.length; i++)
    {
      if (str == this.wordsToFind[i].getWordString())
      {
         for (let j = 0; j < this.tempLetters.length; j++)
         {
            this.letters[this.tempLetters[j].getX()][this.tempLetters[j].getY()].placeInWord();
         }
         this.wordsToFind[i].setFoundStatus();
         this.calculateScore(str.length);
        //Check totalWords Found to see if new game needs to be created:
        let endStage:boolean = true;
        for(let n = 0; n < this.wordsToFind.length; n++)
        {
          if(this.wordsToFind[n].getFoundStatus() == false)
          {
            console.log('not ending yet');
            endStage = false;
            break;
          }
        }
        if(endStage)
        {
          this.loadingMessage = "Loading Next Level";
          console.log('ended level');
          //logic to end game:
          this.initialize();
        }
      }
    }

    this.mouseIsDown = false;
    this.removeHighFromAllLetters();
    this.tempString = null;
  }

  calculateScore(number){
      this.playerScore += (1000 - (number * 50)) + (2 * this.secondsLeft);
  }
  removeHighFromAllLetters()
  {
    for(let i: number = 0; i < 12; i++)
    {
            for(let j: number = 0; j < 12; j++)
            {
                if (this.letters[i][j].getHigh())
                    this.letters[i][j].removeHigh();
            }
    }
    this.tempLetters = [];
  }
  mouseMovedOnCol(i, j) {
    if (this.mouseIsDown)
    {
      // this.letters[i][j].highlight(); //old one - uncomment this for basic highlighting

        //If first letter selected -> highlight
        if (this.tempLetters.length == 0)
        {
             this.selectLetter(i, j);
             this.xCoord = i;
             this.yCoord = j;
        }
        else if (!this.tempLetters.includes(this.letters[i][j]))
        {
          if (this.checkIfInlineWithFirstLetter(i, j))
          {
              this.selectLetter(i, j);
              this.fillInPreviousLetters(i, j);
          }
          //add else here for hovering on letters functionality
        }
        else
        {
          //logic for already a letter in the list:
          this.selectLetter(i, j);
          this.fillInPreviousLetters(i, j);
        }
     }
  }

  isColHighlighted(i, j)  {
     return this.letters[i][j].getHigh();
  }

  isColInWord(i, j) {
    return this.letters[i][j].getInWord();
  }

  checkIfInlineWithFirstLetter(i, j) {
    for (let p = 1; p < 12; p++)
    {
      if ((Math.abs(i - this.tempLetters[0].getX()) == p) && ((Math.abs(j - this.tempLetters[0].getY())  == 0 )))
      {
        return true;
      }
      else if ((Math.abs(i - this.tempLetters[0].getX()) == p) && ((Math.abs(j - this.tempLetters[0].getY()) == p)))
      {
        return true;
      }
      else if ((Math.abs(i - this.tempLetters[0].getX()) == 0) && ((Math.abs(j - this.tempLetters[0].getY()) == p)))
      {
        return true;
      }
    }
    return false;
  }

  fillInPreviousLetters(i, j) {
    this.removeHighFromAllLetters();
    this.tempLetters.push(this.letters[this.xCoord][this.yCoord]);
    this.letters[this.xCoord][this.yCoord].highlight();

    //first remove all elements from tempLetters but the first one.
    // this.tempLetters.splice(1);
    let tempX = this.xCoord;
    let tempY = this.yCoord;

    //find greatest difference between XCoord or YCoord from origin
    let difference = Math.abs(this.xCoord - i);
    if (difference < Math.abs(this.yCoord - j))
    {
      difference = Math.abs(this.yCoord - j);
    }

    for (let d = 0; d < difference; d++)
    {
      if (tempX != i)
      {
        if (tempX < i)
        {
          tempX++;
        }
        else {
          tempX--;
        }
      }
      if (tempY != j)
      {
        if (tempY < j)
        {
          tempY++;
        }
        else {
          tempY--;
        }
      }
      this.tempLetters.push(this.letters[tempX][tempY]);
      this.letters[tempX][tempY].highlight();
    }

    //now populate tempLetters with letters in between the last clicked one.
  }

  selectLetter (i, j) {

    let str:string = "";
    for (let d = 0; d < this.tempLetters.length; d++)
    {
      str += this.tempLetters[d].getChar();
    }
    this.tempString = str;
    this.tempLetters.push(this.letters[i][j]);
    this.letters[i][j].highlight();
  }

  navigateToTitle() {
    this.navCtrl.pop();
  }

  isWordFound(i,j){
    var numWord:number = (i * 3) + j;

    if(this.wordsToFind[numWord].getFoundStatus() == true)
      return true;

    return false;
  }

  checkHighScores()  {
    var tempHighScores = [];
    this.databaseProvider.getHighScores((highScoresArray) => {
          if(highScoresArray)
          {
              tempHighScores = highScoresArray;
              console.log(tempHighScores);

              if (tempHighScores.length < 8)
              {
                //Add New Entry into highscores:
                this.databaseProvider.createNewHighScore(this.afAuth.auth.currentUser.displayName, this.playerScore, (addHighScore) => {
                  console.log('added new high score');
                  this.showSuccessResult();
                  console.log('you won');
                  clearInterval(this.timer);
                });
              }
              else {
                    for (let i:number = 0; i < tempHighScores.length; i++)
                    {
                        //Already 10 highscores and made the leaderboard:
                        if (this.playerScore > tempHighScores[i][0])
                        {
                            //Delete lowest score:
                            this.madeHighScores = true;
                            tempHighScores = this.sortHighScores(tempHighScores);
                            console.log(tempHighScores);
                            console.log('the one being deleted is: ' + tempHighScores[tempHighScores.length-1][0]);
                            this.databaseProvider.deleteHighScore(tempHighScores[tempHighScores.length-1][0], (test) => {
                              console.log('deleted highscore');
                            });
                            this.databaseProvider.createNewHighScore(this.afAuth.auth.currentUser.displayName, this.playerScore, (test) => {
                                  console.log('added new high score');
                                  this.showSuccessResult();
                                  console.log('you won');
                                  clearInterval(this.timer);
                            });

                            break;
                         }
                     }
               }
               if (this.madeHighScores == false)
               {
                 this.showFailureResult();
                 clearInterval(this.timer);
               }
          }
          else
          {
              console.log('empty array');
          }
        });
    }



    sortHighScores(tempArray){
      var swapped:boolean;
      do {
          swapped = false;
          for (var i=0; i < tempArray.length-1; i++) {
              if (+tempArray[i][0] < +tempArray[i+1][0]) {
                  var temp = tempArray[i];
                  tempArray[i] = tempArray[i+1];
                  tempArray[i+1] = temp;
                  swapped = true;
              }
          }
      } while (swapped);
      return tempArray;
    }

    showSuccessResult() {
       const alert = this.alertCtrl.create({
       title: 'SUCCESS!',
       subTitle: 'Your score of ' + this.playerScore + ' made the leaderboards !',
       buttons: [{
         text: 'Go To LeaderBoards',
         handler: data => {
           this.navCtrl.pop();
           this.navCtrl.push(HighscoresPage);
         }
       }],
       cssClass: 'alertCustomCss' // <- added this
     });
     alert.present();
     }

     showFailureResult() {
        const alert = this.alertCtrl.create({
        title: 'Game Over',
        subTitle: 'Sorry your score of ' + this.playerScore + ' didnt make the leaderboards !',
        buttons: [{
          text: 'Play Again:',
          handler: data => {
            this.initialize();
            this.startCountDown();
            this.playerScore = 0;
          }
        },
      {
        text: 'Home',
        handler: data => {
          this.navCtrl.pop();
        }
      }],
        cssClass: 'alertCustomCss' // <- added this
      });
      alert.present();
      }

}
