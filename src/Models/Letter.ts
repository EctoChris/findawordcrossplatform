import { LetterUtilProvider } from '../providers/letter-util/letter-util';


export class Letter {

   //Class Data Members:
   highlighted:boolean;
   hovered:boolean;
   inWord:boolean;
   private char;
   public x:number;
   public y:number;

  constructor(private letterUtil: LetterUtilProvider, x:number, y:number)
  {
    this.highlighted = false;
    this.hovered = false;
    this.inWord = false;
    this.char = "0";
    this.x = x;
    this.y = y;
  }

  highlight()
  {
    this.highlighted = true;
  }

  getHigh()
  {
    return this.highlighted;
  }

  getInWord()
  {
    return this.inWord;
  }

  getHover()
  {
    return this.hovered;
  }

  hoverOnLetter()
  {
    this.hovered = true;
  }
  removeHigh()
  {
    this.highlighted = false;
  }
  placeInWord()
  {
    this.inWord = true;
  }

  getChar()
  {
      return this.char;
  }

  setChar(char){
    this.char = char;
  }
  getX()
  {
    return this.x;
  }
  getY()
  {
    return this.y;
  }
}
