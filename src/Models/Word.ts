


export class Word {

  private wordString:string;
  private wordFound:boolean;

constructor(){
  this.wordFound = false;
  this.wordString = "generatingWord";
}

getFoundStatus(){
  return this.wordFound;
}

setFoundStatus(){
  this.wordFound = true;
}

setWordString(newString) {
  this.wordString = newString;
}

getWordString(){
  return this.wordString;
}

}
